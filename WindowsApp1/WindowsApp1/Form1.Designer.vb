﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class HotSpotFinder
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.hsFinder = New System.Windows.Forms.Button()
        Me.txtFileDirectory = New System.Windows.Forms.TextBox()
        Me.btnBrowse = New System.Windows.Forms.Button()
        Me.Lmoran = New System.Windows.Forms.RadioButton()
        Me.Lg = New System.Windows.Forms.RadioButton()
        Me.Kde = New System.Windows.Forms.RadioButton()
        Me.Stac = New System.Windows.Forms.RadioButton()
        Me.Nnhc = New System.Windows.Forms.RadioButton()
        Me.fileNameTxt = New System.Windows.Forms.Label()
        Me.filePathTxt = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.LmoranTime = New System.Windows.Forms.Label()
        Me.Gtime = New System.Windows.Forms.Label()
        Me.kdetime = New System.Windows.Forms.Label()
        Me.stactime = New System.Windows.Forms.Label()
        Me.nnhctime = New System.Windows.Forms.Label()
        Me.exitBtn = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'hsFinder
        '
        Me.hsFinder.Enabled = False
        Me.hsFinder.Location = New System.Drawing.Point(533, 38)
        Me.hsFinder.Name = "hsFinder"
        Me.hsFinder.Size = New System.Drawing.Size(127, 23)
        Me.hsFinder.TabIndex = 0
        Me.hsFinder.Text = "Find Hot Spots"
        Me.hsFinder.UseVisualStyleBackColor = True
        '
        'txtFileDirectory
        '
        Me.txtFileDirectory.Location = New System.Drawing.Point(12, 12)
        Me.txtFileDirectory.Name = "txtFileDirectory"
        Me.txtFileDirectory.Size = New System.Drawing.Size(648, 20)
        Me.txtFileDirectory.TabIndex = 1
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(12, 38)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(75, 23)
        Me.btnBrowse.TabIndex = 2
        Me.btnBrowse.Text = "Browse"
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'Lmoran
        '
        Me.Lmoran.AutoSize = True
        Me.Lmoran.Location = New System.Drawing.Point(13, 77)
        Me.Lmoran.Name = "Lmoran"
        Me.Lmoran.Size = New System.Drawing.Size(84, 17)
        Me.Lmoran.TabIndex = 5
        Me.Lmoran.TabStop = True
        Me.Lmoran.Text = "Local Moran"
        Me.Lmoran.UseVisualStyleBackColor = True
        '
        'Lg
        '
        Me.Lg.AutoSize = True
        Me.Lg.Location = New System.Drawing.Point(225, 77)
        Me.Lg.Name = "Lg"
        Me.Lg.Size = New System.Drawing.Size(89, 17)
        Me.Lg.TabIndex = 6
        Me.Lg.TabStop = True
        Me.Lg.Text = "Getis Local G"
        Me.Lg.UseVisualStyleBackColor = True
        '
        'Kde
        '
        Me.Kde.AutoSize = True
        Me.Kde.Location = New System.Drawing.Point(463, 77)
        Me.Kde.Name = "Kde"
        Me.Kde.Size = New System.Drawing.Size(144, 17)
        Me.Kde.TabIndex = 7
        Me.Kde.TabStop = True
        Me.Kde.Text = "Kernel Density Estimation"
        Me.Kde.UseVisualStyleBackColor = True
        '
        'Stac
        '
        Me.Stac.AutoSize = True
        Me.Stac.Location = New System.Drawing.Point(13, 113)
        Me.Stac.Name = "Stac"
        Me.Stac.Size = New System.Drawing.Size(53, 17)
        Me.Stac.TabIndex = 8
        Me.Stac.TabStop = True
        Me.Stac.Text = "STAC"
        Me.Stac.UseVisualStyleBackColor = True
        '
        'Nnhc
        '
        Me.Nnhc.AutoSize = True
        Me.Nnhc.Location = New System.Drawing.Point(225, 113)
        Me.Nnhc.Name = "Nnhc"
        Me.Nnhc.Size = New System.Drawing.Size(56, 17)
        Me.Nnhc.TabIndex = 9
        Me.Nnhc.TabStop = True
        Me.Nnhc.Text = "NNHC"
        Me.Nnhc.UseVisualStyleBackColor = True
        '
        'fileNameTxt
        '
        Me.fileNameTxt.AutoSize = True
        Me.fileNameTxt.Location = New System.Drawing.Point(102, 56)
        Me.fileNameTxt.Name = "fileNameTxt"
        Me.fileNameTxt.Size = New System.Drawing.Size(54, 13)
        Me.fileNameTxt.TabIndex = 10
        Me.fileNameTxt.Text = "File Name"
        '
        'filePathTxt
        '
        Me.filePathTxt.AutoSize = True
        Me.filePathTxt.Location = New System.Drawing.Point(102, 43)
        Me.filePathTxt.Name = "filePathTxt"
        Me.filePathTxt.Size = New System.Drawing.Size(48, 13)
        Me.filePathTxt.TabIndex = 11
        Me.filePathTxt.Text = "File Path"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 182)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 13)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Local Moran:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 219)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 13)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Getis Local G:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 255)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(84, 13)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "Kenrnel Density:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(16, 296)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(38, 13)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "STAC:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(20, 336)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 13)
        Me.Label5.TabIndex = 16
        Me.Label5.Text = "NNHC:"
        '
        'LmoranTime
        '
        Me.LmoranTime.AutoSize = True
        Me.LmoranTime.Location = New System.Drawing.Point(192, 182)
        Me.LmoranTime.Name = "LmoranTime"
        Me.LmoranTime.Size = New System.Drawing.Size(0, 13)
        Me.LmoranTime.TabIndex = 17
        '
        'Gtime
        '
        Me.Gtime.AutoSize = True
        Me.Gtime.Location = New System.Drawing.Point(192, 219)
        Me.Gtime.Name = "Gtime"
        Me.Gtime.Size = New System.Drawing.Size(0, 13)
        Me.Gtime.TabIndex = 18
        '
        'kdetime
        '
        Me.kdetime.AutoSize = True
        Me.kdetime.Location = New System.Drawing.Point(192, 255)
        Me.kdetime.Name = "kdetime"
        Me.kdetime.Size = New System.Drawing.Size(0, 13)
        Me.kdetime.TabIndex = 19
        '
        'stactime
        '
        Me.stactime.AutoSize = True
        Me.stactime.Location = New System.Drawing.Point(192, 296)
        Me.stactime.Name = "stactime"
        Me.stactime.Size = New System.Drawing.Size(0, 13)
        Me.stactime.TabIndex = 20
        '
        'nnhctime
        '
        Me.nnhctime.AutoSize = True
        Me.nnhctime.Location = New System.Drawing.Point(186, 336)
        Me.nnhctime.Name = "nnhctime"
        Me.nnhctime.Size = New System.Drawing.Size(0, 13)
        Me.nnhctime.TabIndex = 21
        '
        'exitBtn
        '
        Me.exitBtn.Location = New System.Drawing.Point(533, 331)
        Me.exitBtn.Name = "exitBtn"
        Me.exitBtn.Size = New System.Drawing.Size(127, 23)
        Me.exitBtn.TabIndex = 22
        Me.exitBtn.Text = "Exit"
        Me.exitBtn.UseVisualStyleBackColor = True
        '
        'HotSpotFinder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(694, 384)
        Me.Controls.Add(Me.exitBtn)
        Me.Controls.Add(Me.nnhctime)
        Me.Controls.Add(Me.stactime)
        Me.Controls.Add(Me.kdetime)
        Me.Controls.Add(Me.Gtime)
        Me.Controls.Add(Me.LmoranTime)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.filePathTxt)
        Me.Controls.Add(Me.fileNameTxt)
        Me.Controls.Add(Me.Nnhc)
        Me.Controls.Add(Me.Stac)
        Me.Controls.Add(Me.Kde)
        Me.Controls.Add(Me.Lg)
        Me.Controls.Add(Me.Lmoran)
        Me.Controls.Add(Me.btnBrowse)
        Me.Controls.Add(Me.txtFileDirectory)
        Me.Controls.Add(Me.hsFinder)
        Me.Name = "HotSpotFinder"
        Me.Text = "Hot Spot Finder"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents hsFinder As Button
    Friend WithEvents txtFileDirectory As TextBox
    Friend WithEvents btnBrowse As Button
    Friend WithEvents Lmoran As RadioButton
    Friend WithEvents Lg As RadioButton
    Friend WithEvents Kde As RadioButton
    Friend WithEvents Stac As RadioButton
    Friend WithEvents Nnhc As RadioButton
    Friend WithEvents fileNameTxt As Label
    Friend WithEvents filePathTxt As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents LmoranTime As Label
    Friend WithEvents Gtime As Label
    Friend WithEvents kdetime As Label
    Friend WithEvents stactime As Label
    Friend WithEvents nnhctime As Label
    Friend WithEvents exitBtn As Button
End Class
