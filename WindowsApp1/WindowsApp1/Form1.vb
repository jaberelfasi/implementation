﻿Imports System.IO

Public Class HotSpotFinder
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        hsFinder.Enabled = True
    End Sub

    Private Sub hsFinder_Click(sender As Object, e As EventArgs) Handles hsFinder.Click

        If Lmoran.Checked Then
            MsgBox("Local Moran")
            localMoran()
        ElseIf Lg.Checked Then
            MsgBox("L g")
            localG()
        ElseIf Kde.Checked Then
            MsgBox("kde")
            kernelDensity()
        ElseIf Nnhc.Checked Then
            MsgBox("NNHC")
            nnhHotspots()
        ElseIf Stac.Checked Then
            MsgBox("stac")
            stacHotspots()
        End If


    End Sub

    Private Sub btnBrowse_Click(sender As Object, e As EventArgs) Handles btnBrowse.Click
        Dim myFileDlog As New OpenFileDialog()

        'look for files in the c drive
        myFileDlog.InitialDirectory = "c:\"

        'specifies what type of data files to look for
        myFileDlog.Filter = "All Files (*.Shp)|*.Shp"

        'specifies which data type is focused on start up
        myFileDlog.FilterIndex = 2

        'Gets or sets a value indicating whether the dialog box restores the current directory before closing.
        myFileDlog.RestoreDirectory = True

        'seperates message outputs for files found or not found
        If myFileDlog.ShowDialog() =
            DialogResult.OK Then
            If Dir(myFileDlog.FileName) <> "" Then
                MsgBox("File Exists: " &
                       myFileDlog.FileName,
                       MsgBoxStyle.Information)
                hsFinder.Enabled = True
            Else
                MsgBox("File Not Found",
                       MsgBoxStyle.Critical)
            End If
        End If

        'Adds the file directory to the text box
        txtFileDirectory.Text = myFileDlog.FileName
        filePathTxt.Text = Path.GetDirectoryName(myFileDlog.FileName)
        fileNameTxt.Text = Path.GetFileNameWithoutExtension(myFileDlog.FileName) 'System.IO.Path.GetFileName(myFileDlog.FileName)
    End Sub

    Private Sub localMoran()

        'Create Object
        Dim file As New CInputParam
        'Initialize values
        file.m_iDistanceType = Utilities.DISTANCE_TYPE.DISTANCE_TYPE_PROJECTED
        file.m_iDataUnit = Utilities.UNIT_TYPE.UNIT_TYPE_METERS
        file.m_iMeasureType = Utilities.MEASURE_TYPE.MEASURE_TYPE_DIRECT
        file.m_iTimeUnit = Utilities.TIME_UNIT_TYPE.TIME_UNIT_TYPE_MONTHS

        'Add Primary File
        file.m_fileType = FILE_TYPE.FILE_TYPE_SHAPE
        Dim fileid As Integer
        fileid = file.AddPrimaryFile(txtFileDirectory.Text)

        file.SetX(fileid, 0)
        file.SetY(fileid, 1)
        file.SetIntensity(fileid, 3)

        file.UpdatePrimaryData()

        Dim kLocalMoran As New CCalcLocalMoran
        kLocalMoran.Initialize(file.m_pPrimaryPointSet, False, True, 0)

        'Declare three variables
        Dim start_time As DateTime
        Dim end_time As DateTime
        start_time = Now
        kLocalMoran.PerformCalculations()
        end_time = Now


        Dim currentPath = filePathTxt.Text + "/CrimeStatOutput/" + fileNameTxt.Text + "LMoranI.csv"
        If My.Computer.FileSystem.FileExists(currentPath) Then
            My.Computer.FileSystem.DeleteFile(currentPath)
        End If

        Dim objfirst As New StreamWriter(currentPath)
        objfirst.Write("Cluster" + "," + "X Co-ordinate" + "," + "Y Coordinate" + "," + "I" + "," + "Expt I")
        objfirst.WriteLine()
        objfirst.Close()

        Dim s As String
        Dim i As Integer
        For i = 0 To kLocalMoran.GetRowCount() - 1 Step 1
            s = String.Format(" {0},{1:F6},{2:F6},{3:F6},{4:F6}", i +
            1, kLocalMoran.GetX(i), kLocalMoran.GetY(i), kLocalMoran.GetI(i), kLocalMoran.GetExpectedI(i))
            Dim objwri As New StreamWriter(currentPath, True)
            objwri.WriteLine(s)
            objwri.Close()
        Next i



        Dim interval = end_time.Subtract(start_time)
        LmoranTime.Text = "Total Seconds (" + interval.TotalSeconds.ToString("0.000000") + ")"

        MsgBox("finished")
    End Sub

    Private Sub localG()
        'Create Object
        Dim file As New CInputParam


        'Initialize values
        file.m_iDistanceType = Utilities.DISTANCE_TYPE.DISTANCE_TYPE_PROJECTED
        file.m_iDataUnit = Utilities.UNIT_TYPE.UNIT_TYPE_METERS
        file.m_iTimeUnit = Utilities.TIME_UNIT_TYPE.TIME_UNIT_TYPE_MONTHS
        file.m_iMeasureType = Utilities.MEASURE_TYPE.MEASURE_TYPE_DIRECT

        'Add Primary File
        file.m_fileType = FILE_TYPE.FILE_TYPE_SHAPE
        Dim fileid As Integer
        fileid = file.AddPrimaryFile(txtFileDirectory.Text)
        file.SetX(fileid, 0)
        file.SetY(fileid, 1)
        file.SetIntensity(fileid, 3)

        'Update Data after mapping columns
        file.UpdatePrimaryData()

        Dim localGetisG As New CCalcGetisOrdLocalZone
        localGetisG.Initialize(file.m_pPrimaryPointSet, 0.5,
        Utilities.COV_UNIT_TYPE.COV_UNIT_TYPE_KILOMETERS, 0)

        Dim start_time As DateTime
        Dim end_time As DateTime


        start_time = Now
        localGetisG.PerformCalculations()
        end_time = Now
        Dim interval = end_time.Subtract(start_time)

        Dim currentPath = filePathTxt.Text + "/CrimeStatOutput/" + fileNameTxt.Text + "LocalG.csv"
        If My.Computer.FileSystem.FileExists(currentPath) Then
            My.Computer.FileSystem.DeleteFile(currentPath)
        End If

        Dim objfirst As New StreamWriter(currentPath)
        objfirst.Write("Cluster" + "," + "X Co-ordinate" + "," + "Y Coordinate" + "," + "G" + "," + "Expt G")
        objfirst.WriteLine()
        objfirst.Close()
        Dim i As Integer
        Dim s As String
        For i = 0 To localGetisG.GetRowCount() - 1 Step 1
            s = String.Format(" {0},{1:F6},{2:F6},{3:F6},{4:F6}", i +
            1, localGetisG.GetX(i), localGetisG.GetY(i), localGetisG.GetG(i), localGetisG.GetExpectedG(i))
            Dim objwri As New StreamWriter(currentPath, True)
            objwri.WriteLine(s)
            objwri.Close()
        Next i


        Gtime.Text = "Total Seconds (" + interval.TotalSeconds.ToString() + ")"

        MsgBox("finished")
    End Sub

    Private Sub kernelDensity()
        'Create Object
        Dim file As New CInputParam

        'Initialize values
        file.m_iDistanceType = Utilities.DISTANCE_TYPE.DISTANCE_TYPE_PROJECTED
        file.m_iDataUnit = Utilities.UNIT_TYPE.UNIT_TYPE_METERS
        file.m_iTimeUnit = Utilities.TIME_UNIT_TYPE.TIME_UNIT_TYPE_MONTHS
        file.CreateReferenceFile(350280, 380389, 412088, 421961)

        file.m_iMeasureType = Utilities.MEASURE_TYPE.MEASURE_TYPE_DIRECT

        'Add Primary File
        file.m_fileType = FILE_TYPE.FILE_TYPE_SHAPE
        Dim fileid As Integer
        fileid = file.AddPrimaryFile(txtFileDirectory.Text)
        file.SetX(fileid, 0)
        file.SetY(fileid, 1)
        file.SetIntensity(fileid, 3)

        'Update Data after mapping columns
        file.UpdatePrimaryData()
        file.UpdateReferenceData()


        Dim kernelDenParam As New CCalcKernelDensityParams
        Dim kernelden As New CCalcKernelDensity
        kernelDenParam.setKernelSingleUnit(Utilities.COV_UNIT_TYPE.COV_UNIT_TYPE_KILOMETERS)
        kernelDenParam.setKernelSingleSource(Interpolation.KERNEL_SOURCE.KERNEL_SOURCE_PRIMARY)
        kernelDenParam.setKernelSingleMethod(Interpolation.KERNEL_DENSITY_CALC.KERNEL_DENSITY_CALC_NORMAL)
        kernelDenParam.setKernelSingleBandwidth(Interpolation.KERNEL_BANDWIDTH.KERNEL_BANDWIDTH_ADAPTIVE)
        kernelDenParam.setKernelSingleMinSample(15)
        kernelDenParam.setKernelSingleWeight(False)
        kernelDenParam.setKernelSingleIntensity(True)
        kernelden.Initialize(file.m_pPrimaryPointSet, file.m_pPrimaryPointSet,
                            file.m_pReferencePointSet, kernelDenParam)


        'Dim savefilename As String
        Dim currentPath = filePathTxt.Text + "/CrimeStatOutput/" + fileNameTxt.Text + "KDE.shp"
        If My.Computer.FileSystem.FileExists(currentPath) Then
            My.Computer.FileSystem.DeleteFile(currentPath)
        End If

        Dim start_time As DateTime
        Dim end_time As DateTime


        start_time = Now
        kernelden.PerformCalculations(2, currentPath)
        end_time = Now
        Dim interval = end_time.Subtract(start_time)

        Dim kernelDensityRes() As Double
        kernelDensityRes = kernelden.getDensityValues()
        System.Console.WriteLine("*********************************Printing Results for Kernel Density Estimation*********************************")
        Dim i As Integer
        For i = 0 To kernelden.getNumberOfDensityPoints() - 1 Step 1
            System.Console.WriteLine("Density for Location {0:D} {1:E}", i,
            kernelDensityRes(i))
        Next i
        System.Console.WriteLine("*******************************End Printing Results
                                   for Kernel Density Estimation*********************************")
        kdetime.Text = "Total Seconds (" + interval.TotalSeconds.ToString("0.000000") + ")"

        MsgBox("finished")
    End Sub

    Private Sub nnhHotspots()
        'Create Object
        Dim file As New CInputParam


        'Initialize values
        file.m_iDistanceType = Utilities.DISTANCE_TYPE.DISTANCE_TYPE_PROJECTED
        file.m_iDataUnit = Utilities.UNIT_TYPE.UNIT_TYPE_METERS
        file.m_iTimeUnit = Utilities.TIME_UNIT_TYPE.TIME_UNIT_TYPE_MONTHS
        file.m_iMeasureType = Utilities.MEASURE_TYPE.MEASURE_TYPE_DIRECT


        'Add Primary File
        file.m_fileType = FILE_TYPE.FILE_TYPE_SHAPE
        Dim fileid As Integer
        fileid = file.AddPrimaryFile(txtFileDirectory.Text)
        file.SetX(fileid, 0)
        file.SetY(fileid, 1)

        ''Update Data after mapping columns
        file.UpdatePrimaryData()

        Dim currentPath = filePathTxt.Text + "/CrimeStatOutput/" + fileNameTxt.Text + "NNH.shp"
        If My.Computer.FileSystem.FileExists(currentPath) Then
            My.Computer.FileSystem.DeleteFile(currentPath)
        End If
        Dim nSimRuns As Integer
        nSimRuns = 0
        'params: pointset, min points per cluster, output unit, search raduis significance, standard Deviation

        Dim nnh As New CCalcClusterNNH
        nnh.initialize(file.m_pPrimaryPointSet, 15,
                       Utilities.COV_UNIT_TYPE.COV_UNIT_TYPE_KILOMETERS, 0.5, 2)
        nnh.setSimulationRuns(0)
        nnh.SaveEllipses(currentPath, 2, "Earth Projection", 1, 33)




        'Dim nnh_dbf As New CFileXBaseWrap
        'Dim fileutils As New CFileUtilsPub






        Dim start_time As DateTime
        Dim end_time As DateTime


        start_time = Now
        nnh.PerformCalculations()
        end_time = Now
        Dim interval = end_time.Subtract(start_time)

        'nnh_dbf.Close()
        System.Console.WriteLine("*********************************Printing Results for
                                    NNH*********************************")
        System.Console.WriteLine("No of Ellipses {0:D}", nnh.GetClusterCount())
        Dim clusterQ As New Queue
        clusterQ = nnh.GetClusters()
        System.Console.WriteLine(" Order " & vbTab & " Cluster " & vbTab & " Mean X " &
                                   vbTab & " Mean Y " & vbTab & " Rotation " & vbTab & " X-Axis " & vbTab & " YAxis
                                 " & vbTab & " Area " & vbTab & " Points " & vbTab & " Density ")
        Dim i As Integer
        For i = 0 To nnh.GetClusterCount() - 1 Step 1
            Dim rc As ResultCluster
            rc = clusterQ.Dequeue()
            System.Console.WriteLine(" {0}" & vbTab & "{1}" & vbTab & "{2:F6}" & vbTab &
                                     "{3:F6}" & vbTab & "{4:F6}" & vbTab & "{5:F6}" & vbTab & "{6:F6}" & vbTab &
                                     "{7:F6}" & vbTab & "{8}" & vbTab & "{9:F6}", rc.nOrder, rc.nCluster, rc.dMeanX,
                                     rc.dMeanY, rc.dAngle, rc.dXAxis, rc.dYAxis, rc.dArea, rc.nPoints, rc.dDensity)
        Next i

  
        If nSimRuns > 0 Then
            Dim simArea As Double()
            Dim simClust As Integer()
            Dim simPoints As Integer()
            Dim simDen As Double()
            simArea = nnh.GetSimulationArea()
            simClust = nnh.GetSimulationClusters()
            simPoints = nnh.GetSimulationPoints()
            simDen = nnh.GetSimulationDensity()
            System.Console.WriteLine(" Clusters " & vbTab & " Area " & vbTab & " Points " & vbTab & " Density ")
            For i = 0 To 10 Step 1
                System.Console.WriteLine(" {0}" & vbTab & "{1:F6}" & vbTab & "{2}" &
                vbTab & "{3:F6}", simClust(i), simArea(i), simPoints(i), simDen(i))
            Next i
        End If
        System.Console.WriteLine("*********************************End Printing
                                    Results for NNH*********************************")

        nnhctime.Text = "Total Seconds (" + interval.TotalSeconds.ToString("0.000000") + ")"

        MsgBox("finished")
    End Sub

    Private Sub stacHotspots()
        'Create Object
        Dim file As New CInputParam


        'Initialize values
        file.m_iDistanceType = Utilities.DISTANCE_TYPE.DISTANCE_TYPE_PROJECTED
        file.m_iDataUnit = Utilities.UNIT_TYPE.UNIT_TYPE_METERS
        file.m_iTimeUnit = Utilities.TIME_UNIT_TYPE.TIME_UNIT_TYPE_MONTHS
        file.m_iMeasureType = Utilities.MEASURE_TYPE.MEASURE_TYPE_DIRECT
        file.CreateReferenceFile(350280, 380389, 412088, 421961)
        'Add Primary File
        file.m_fileType = FILE_TYPE.FILE_TYPE_SHAPE
        Dim fileid As Integer
        fileid = file.AddPrimaryFile(txtFileDirectory.Text) 'fileid = file.AddPrimaryFile("C:/Users/jaber/iNoteBooks/ManchesterHotSpotTechniques/data/1_2017ViolenceAndSex.shp")

        file.SetX(fileid, 0)

        file.SetY(fileid, 1)


        ''Update Data after mapping columns
        file.UpdatePrimaryData()
        file.UpdateReferenceData()


        Dim currentPath = filePathTxt.Text + "/CrimeStatOutput/" + fileNameTxt.Text + "STAC.shp"
        If My.Computer.FileSystem.FileExists(currentPath) Then
            My.Computer.FileSystem.DeleteFile(currentPath)
        End If


        Dim stacParam As New CStacParams
        Dim stac As New CCalcStac
        stac.SaveEllipses(currentPath, 2, "Earth Projection", 1, 33)
        stacParam.m_dScanRadius = 0.5
        stacParam.m_nMinPointPerCluster = 15
        stacParam.m_dEllipseStdDev = 2
        stacParam.m_nScanType = STAC_SCAN_TYPE.STAC_SCAN_TYPE_RECTANGULAR
        stacParam.m_nSearchRadiusUnit = Utilities.COV_UNIT_TYPE.COV_UNIT_TYPE_KILOMETERS
        stacParam.m_nStacOutputUnit = Utilities.COV_UNIT_TYPE.COV_UNIT_TYPE_KILOMETERS
        stac.Initialize(file.m_pPrimaryPointSet, file.m_pSecondaryPointSet, stacParam)
        stac.setSimulationRuns(0)




        Dim start_time As DateTime
        Dim end_time As DateTime

        start_time = Now
        stac.PerformCalculations()
        end_time = Now
        Dim interval = end_time.Subtract(start_time)


        Dim clusters() As ResultCluster
        clusters = stac.GetResultClusters
        Dim StrArray() As String = {"Min", "0.5", "1.0", "2.5", "5.0", "10.0", "90.0", "95.0",
                                    "97.5", "99.0", "99.5", "Max"}
        Dim i As Integer
        For i = 0 To clusters.Length - 1 Step 1
            System.Console.WriteLine("Printing Cluster {0}", i + 1)
            System.Console.WriteLine("Mean X {0:F6}", clusters(i).dMeanX)
            System.Console.WriteLine("Mean Y {0:F6}", clusters(i).dMeanY)
            System.Console.WriteLine("Rotation {0:F6}", clusters(i).dAngle)
            System.Console.WriteLine("X-Axis {0:F6}", clusters(i).dXAxis)
            System.Console.WriteLine("Y-Axis {0:F6}", clusters(i).dYAxis)
            System.Console.WriteLine("Points {0:F6}", clusters(i).nPoints)
            System.Console.WriteLine("Area {0:F6}", clusters(i).dArea)
            System.Console.WriteLine("Density {0:F6}", clusters(i).dDensity)
            System.Console.WriteLine("-----------------------------------")
        Next i
        'Dim simclusters() As CResultSimulationClusters
        'simclusters = stac.GetSimulationPercentiles()
        'For i = 0 To 11 Step 1
        '    System.Console.WriteLine("Percentile {0}", StrArray(i))
        '    System.Console.WriteLine("Clusters {0:D}", simclusters(i).nClusters)
        '    System.Console.WriteLine("Area {0:F6}", simclusters(i).dArea)
        '    System.Console.WriteLine("Points {0:D}", simclusters(i).nPoints)
        '    System.Console.WriteLine("Density {0:F6}", simclusters(i).dDensity)
        '    System.Console.WriteLine("-----------------------------------")
        'Next i

        stactime.Text = "Total Seconds (" + interval.TotalSeconds.ToString("0.000000") + ")"

        MsgBox("finished")
    End Sub

    Private Sub exitBtn_Click(sender As Object, e As EventArgs) Handles exitBtn.Click
        Application.Exit()
    End Sub
End Class
